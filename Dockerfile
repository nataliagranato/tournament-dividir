FROM httpd:2.4.46
RUN sed -i '1c#!/usr/bin/perl' /usr/local/apache2/cgi-bin/printenv
RUN chmod +x /usr/local/apache2/cgi-bin/printenv
LABEL maintainer="Natalia Granato"
ENV HTTPD_VERSION 1.0.0
COPY ./app/dividir.sh /usr/local/apache2/cgi-bin
COPY ./app/infodividir.sh /usr/local/apache2/cgi-bin
RUN chmod +x /usr/local/apache2/cgi-bin/dividir.sh
RUN chmod +x /usr/local/apache2/cgi-bin/infodividir.sh
CMD httpd-foreground -c "LoadModule cgid_module modules/mod_cgid.so"
EXPOSE 80
